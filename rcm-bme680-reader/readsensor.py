#  Copyright (c) 2020 Pim Hazebroek
#  This program is made available under the terms of the MIT License.
#  For full details check the LICENSE file at the root of the project.

import logging
import sys
from logging.handlers import RotatingFileHandler

import adafruit_bme680
import board
import busio
import requests
from dotenv import load_dotenv

load_dotenv()
import os

# Import urllib3 and disable warnings about unsecure connection, since HTTPS verify is set to False due to self signed certificate.
import urllib3

urllib3.disable_warnings()

CONTENT_TYPE = 'application/json'
LOG_PATH = '/home/pi/bme680.log'


def init_logging():
    """Initializes the logger. The logfile will be rotated when it reaches 1 Mb. Max 5 copies kept as backup.
    :return: a reference to the logger
    """
    logger = logging.getLogger()
    handler = RotatingFileHandler(LOG_PATH, maxBytes=1048576, backupCount=5)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    logger.setLevel(logging.INFO)
    logger.addHandler(handler)
    return logger


def get_config():
    """Returns the configuration with the (environment) variables needed to run this script. """
    config = {
        'station_api': os.getenv('station-api'),
        'station_name': os.getenv('station-name'),
        'auth0_domain': os.getenv('auth0-domain'),
        'client_id': os.getenv('client-id'),
        'client_secret': os.getenv('client-secret'),
        'audience': os.getenv('audience'),
        'auth_enabled': os.getenv('auth-enabled', 'True')
    }

    if config['auth_enabled'] == 'True' and (
            config['auth0_domain'] is None
            or config['client_id'] is None
            or config['client_secret'] is None
            or config['audience'] is None):
        raise ValueError("Auth is not configured properly. Either disabled auth explicitly or check the configuration.")

    return config


def get_token():
    """Retrieve the access token for M2M authentication on the station monitoring service. """
    if config['auth_enabled'] == 'False':
        return None

    uri = f'https://{config["auth0_domain"]}/oauth/token'

    payload = {
        'grant_type': 'client_credentials',
        'client_id': config['client_id'],
        'client_secret': config['client_secret'],
        'audience': config['audience']
    }

    try:
        response = requests.post(uri, data=payload)

        if response.status_code != 200:
            log.error('Failed to authenticate. Response: %s', response.text)
            sys.exit(401)

        data = response.json()
    except requests.exceptions.ConnectionError:
        log.exception("Failed to retrieve access token")
        sys.exit(401)

    return data['access_token']


def submit_report(measurements):
    """Submits the measurement report to the station service API.
        :param measurements a key-value map with the measurements. Each metric has key is with the measuring as value.
    """
    uri = f'{config["station_api"]}/measurement-reports'

    payload = {
        'stationName': config['station_name'],
        'measurements': measurements
    }

    headers = {
        'content-type': 'application/json',
        'Authorization': f'Bearer {token}'
    }

    try:
        response = requests.post(uri, json=payload, headers=headers, verify=False)

        if response.status_code != 204:
            log.error('Failed to submit measurement report. Code: %s', response.status_code)
            sys.exit(response.status_code)

        log.debug('Submitted measurement report: %s. station=%s, target=%s', measurements, config['station_name'], uri)
    except requests.exceptions.ConnectionError:
        log.exception("Failed to submit measurements report")
        sys.exit(999)


def read_sensor():
    """Reads the values from the sensor via I2C"""
    log.debug('Reading BME680 sensor...')
    print('Reading BME680 sensor...')

    i2c = busio.I2C(board.SCL, board.SDA)
    sensor = adafruit_bme680.Adafruit_BME680_I2C(i2c)

    measurements = {
        'temperature': sensor.temperature,
        'humidity': sensor.humidity,
        'air-pressure': sensor.pressure,
        'voc-gas': sensor.gas
    }

    log.info(measurements)
    print(measurements)

    submit_report(measurements)


# Initialize logging so we know what is going on
log = init_logging()

try:
    # Retrieve the external configuration
    config = get_config()

    # Authenticate against the identity provider and obtain the access token
    token = get_token()

    # Actually measure the values of the sensor and send them to the API
    read_sensor()
except:
    log.exception("Unknown error")
