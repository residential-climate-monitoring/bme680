# BME680 - High precision temperature, humidity, pressure and VOC gas sensor

Python module that reads the [Adafruit BME680] sensor and posts the readings to the station monitoring service API.

# Prerequisites

## Install and update raspbian and python

Make sure the pi is running the latest version of Raspbian and Python 3:

```shell script
sudo apt-get update
sudo apt-get upgrade
sudo pip3 install --upgrade setuptools
# If above doesn't work try:
sudo apt-get install python3-pip
```

## Enable I2C

Also make sure I2C is enabled on the Pi. See [enabling I2C]

## Install and run the Station Monitoring Service

Before the sensor can start measurement the sensor and submit a report with the measurements the station monitoring service
should be up and running. Please check the README of the station monitoring service for more details.

---

# Install guide

## Optionally, create a virtual environment:

```shell script
mkdir residential-climate-monitoring && cd residential-climate-monitoring
python3 -m venv .env
source .env/bin/activate
```

## Install python dependencies

Install the following Python libraries:

 
### Install CircutPython:

**Note:** If your default Python is version 3 you may need to run 'pip' instead. Just make sure you aren't trying to use 
CircuitPython on Python 2.x, it isn't supported! 
The instructions below are a short summary of [CircuitPython on Raspberry Pi]

* Install RPI.GPIO:
    This is a prerequisite for the Adafruit CircuitPython library.

    ```shell script
    pip3 install RPI.GPIO
    ```

* Install [Adafruit Blinka Library]:
    This will install the Adafruit CircuitPython library and all the other essential scripts.

    ```shell script
    pip3 install adafruit-blinka
    ```

### Install sensor driver
Install the python library for the [Adafruit BME680] sensor:

```shell script
pip3 install adafruit-circuitpython-bme680
```
  
### Install requests library
The [Requests library] is used to submit measurements reports to the station monitoring service API.
    
```shell script
pip3 install requests
```
  
### Install python-dotenv library
The [python-dotenv library] is used to read variables from the .env file.
    
```shell script
pip3 install python-dotenv
```
  
## Install this script
For now, the script is not yet available as a python library that can be installed from PyPi, instead the script will have to be installed manually:

```shell script
git clone https://gitlab.com/residential-climate-monitoring/bme680.git
```

## Verify everything is working

Run blinkatest.py to check everything works:

```shell script
python3 bme680/rcm-bme680-reader/blinkatest.py
```

You should see the following output:
```shell script
Hello blinka!
Digital IO ok!
I2C ok!
SPI ok!
done!
```

---

# Connect the sensor

To connect the sensor using I2C, wire it as follows: Tip: check [Raspberry GPIO] to see where each pin is on the Pi. 

|RPIO         |Sensor    |
|-------------|----------|
|3.3v         |VIN       |
|GND          |GND       |
|GPIO 2 (SDA) |SDI       |
|GPIO 3 (SCL) |SCK       |

By default the I2C address is 0x77, If you add a jumper from SDO to GND on the sensor, the address will change to 0x76.

# Configuration

The script requires several environment variables to be set. These variables are read by the script from the `.env` file.
The following variables should be set:

* `station-api` - The address where the station monitoring service API is exposed. This is where it will submit the measurement reports to. Example: https://raspberrypi-main.local:8080
* `station-name` - The name under which the station is known to the station monitoring service. Example: my-test-station
* `auth0-domain` - This is where the script will authenticate itself and obtain an access token from. Default: rcm.eu.auth0.com
* `client-id` - The client ID used for authentication. 
* `client-secret` - The secret used for authentication.
* `audience` - Specify the audience to which the authenticated request will be made. This should be validated by the station monitoring service. Default: https://station-monitoring-service

---

# Reading the sensor:

Once all prerequisites are met, the sensor can be read using:

```shell script
python3 bme680/rcm-bme680-reader/readsensor.py
``` 

It should print something like this:

```shell script
Reading BME680 sensor...
{'temperature': 20.313203125, 'humidity': 63.928337396945956, 'air-pressure': 1018.0757158654097, 'voc-gas': 16241}
```

## Reading the sensor on a regular interval

[Crontab] can be used to gather readings from the sensor on a frequent basis.
The benefit of Crontab is that it is easy to configure with a cron expression and will keep working even if the system reboots.

To configure it, enter the following on the terminal:

```shell script
crontab -e
```  
This will open the config file in an editor where the cron expression, and the script to execute are configured.

The following example executes the readsensor.py script once every minute:

```shell script
* * * * * /home/pi/residential-climate-monitoring/.env/bin/python3 /home/pi/residential-climate-monitoring/bme680/rcm-bme680-reader/readsensor.py
```  

Note that it executes the script with python3 from the virtual env created during the installation.
This virtual env contains all the dependencies the script needs.

---

# Troubleshooting

To troubleshoot, the logs can be found at: `/var/log/bme680.log`

# License

The Residential Monitoring Service is distributed under the MIT License

[CircuitPython on Raspberry Pi]: https://learn.adafruit.com/circuitpython-on-raspberrypi-linux
[Enabling I2C]: https://learn.adafruit.com/adafruits-raspberry-pi-lesson-4-gpio-setup/configuring-i2c
[Adafruit BME680]: https://learn.adafruit.com/adafruit-bme680-humidity-temperature-barometic-pressure-voc-gas 
[Requests library]: https://requests.readthedocs.io/en/master/ 
[Adafruit Blinka Library]: https://pypi.org/project/Adafruit-Blinka/
[Raspberry GPIO]: https://www.raspberrypi.org/documentation/usage/gpio/
[Crontab]: https://www.raspberrypi.org/documentation/linux/usage/cron.md
[python-dotenv library]: https://pypi.org/project/python-dotenv/
